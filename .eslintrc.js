module.exports = {
	env: {
		browser: true,
		es6: true
	},
	extends: [
	'plugin:vue/vue3-recommended',
	'plugin:vue/recommended',
	'plugin:vue-scoped-css/recommended',
    'eslint:recommended'
	],
	globals: {
		Atomics: 'readonly',
		SharedArrayBuffer: 'readonly'
	},
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module'
	},
	plugins: [
		'vue'
	],
	rules: {
		'vue/max-attributes-per-line': ['error',{
			'singleline': 6,
			'multiline': {
			'max': 10,
			'allowFirstLine': true}}],
		'vue/component-tags-order': ["warn", { "order": [["template", "script"], "style"] }],	
    	'vue/html-indent': ['error',2,{
			'attribute':1,
			'baseIndent':1,
			'closeBracket':1,
			'alignAttributesVertically': true,
			'ignores':[]
    }]
	}
}