import VuexPersistance from 'vuex-persist'


const state = {
}

const vuexLocal = new VuexPersistance({
    storage: window.localStorage
})

export default {
  state: state,
  modules: {

  },
  plugins: [vuexLocal.plugin]
}