import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import App from './App.vue'
import router from './router'
import vuexLocal from '@/store'
import Vuetify from 'vuetify/lib';

import 'material-design-icons-iconfont/dist/material-design-icons.css' //support for vuetify's use of MD icons
import '@fortawesome/fontawesome-free/css/all.css' 


Vue.config.productionTip = false

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(Vuetify);

const store = new Vuex.Store(vuexLocal)

const vuetify = new Vuetify({
    theme: {
        light: true,
        themes: {
          light: {
            primary: '#e20074',
            secondary: '#e20074',
            accent: '#e20074',
            error: '#e20074',
          },
        },
      },
    icons: {
        iconfont: 'fa',
    }
})

new Vue({
    el: '#app',
    components: { App },
    router,
    store,
    vuetify,
    template: '<App/>'
})