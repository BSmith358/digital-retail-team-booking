module.exports = {
  'runtimeCompiler': true,
  'lintOnSave': true,
  'transpileDependencies': [
    'vuetify'
  ],
  devServer: {
    overlay: {
      warnings: true,
      errors: true
    }
  }
}